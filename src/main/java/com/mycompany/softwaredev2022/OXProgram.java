/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.softwaredev2022;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author a
 */
public class OXProgram {

    public static void main(String[] args) {


        char[][] board = new char[3][3];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }

        Scanner in = new Scanner(System.in);
        System.out.println("Welcome to OX Game");
        String p1 = "X";
        String p2 = "O";

        boolean player1 = true;

        boolean gameEnded = false;
        while (!gameEnded) {

            drawBoard(board);

            if (player1) {
                System.out.println("Turn "+p1);
            } else {
                System.out.println("Turn "+p2);
            }

            char c = '-';
            if (player1) {
                c = 'x';
            } else {
                c = 'o';
            }

            int row = 0;
            int col = 0;

            while (true) {

                System.out.println("Please input row, col:");
                row = in.nextInt();
                col = in.nextInt();

                if (row < 0 || col < 0 || row > 2 || col > 2) {


                } else if (board[row][col] != '-') {


                } else {
                    break;
                }

            }

            board[row][col] = c;

            if (playerHasWon(board) == 'x') {
                System.out.println(">>>X Win<<<");
                gameEnded = true;
            } else if (playerHasWon(board) == 'o') {
                System.out.println(">>>O Win<<<");
                gameEnded = true;
            } else {

                if (Draw(board)) {
                    System.out.println(">>>Draw<<<");
                    gameEnded = true;
                } else {
                    player1 = !player1;
                }

            }

        }

        drawBoard(board);

    }

    public static void drawBoard(char[][] board) {
        for (int i = 0; i < 3; i++) {

            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j]);
            }

            System.out.println();
        }
    }

    public static char playerHasWon(char[][] board) {

        for (int i = 0; i < 3; i++) {
            if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != '-') {
                return board[i][0];
            }
        }

        for (int j = 0; j < 3; j++) {
            if (board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[0][j] != '-') {
                return board[0][j];
            }
        }

        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != '-') {
            return board[0][0];
        }
        if (board[2][0] == board[1][1] && board[1][1] == board[0][2] && board[2][0] != '-') {
            return board[2][0];
        }

        return ' ';

    }

    public static boolean Draw(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
}

